package library.entities;
import java.time.Duration;
import java.time.Instant;
import java.util.Calendar;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import library.entities.IBook.BookState;
import library.entities.ILoan.LoanState;

class LibraryTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testCalculateOverDueFine() {
        //arrange- Expected value is 1 for 1 day overdue
		int expected = 1;
     	Book testBook = new Book("g","GOPAL", "test",1);	
		Patron testPatron = new Patron("BOGATI","GOPAL","GOPAL2736@GMAIL.COM",12345,1);
		Date dueDate = new Date(System.currentTimeMillis() - 1 * 24 * 60 * 60 * 1000);
		Loan loan = new Loan(testBook, testPatron, dueDate, LoanState.OVER_DUE, 1);
	    Library library= new Library(null,null,null,null,null,null,null,null);
		//Assert - checking the function for the actual assert value
		double actual = library.calculateOverDueFine(loan);
		//Act- Expected and actual value check
		assertEquals(expected,actual);
	}
	@Test
	void testCalculateOverDueFineTwoDays() {
        //arrange- Expected value is 2 for 2 days overdue
		int expected = 2;
     	Book testBook = new Book("bh","Hem", "2333",3);	 
		Patron testPatron = new Patron("Khan","Shahrukh","shah@GMAIL.COM",5666,3);
		Date dueDate = new Date(System.currentTimeMillis() - 2 * 24 * 60 * 60 * 1000);
	    Loan loan = new Loan(testBook, testPatron,dueDate , LoanState.OVER_DUE, 3);
		Library library= new Library(null,null,null,null,null,null,null,null);
		//Assert - checking the function for the actual assert value
		double actual = library.calculateOverDueFine(loan);
		//Act- Expected and actual value check
		assertEquals(expected,actual);
	}

}
